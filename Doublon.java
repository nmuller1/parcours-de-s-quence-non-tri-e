/*
 * Muller Noëmie
 * Langage de programmation
 * 000458865
 */

public class Doublon<T> {
	
	private static boolean found;
	//flag qui est mis à true lorsqu'un doublon est trouvé
    
	private static class ThreadTri<T> extends Thread
    //classe qui implémente les threads dédiés au tri du tableau
	{
		int index; //indice de l'élément dont on cherche le doublon
		int begin; //début du sous-tableau concerné par le thread
		int end; //fin du sous-tableau concerné par le thread
		T[] tab; //tableau complet
		
		public ThreadTri(int index, int begin, int end, T[] tab)
		/* Constructeur */
		{
			this.index=index;
			this.begin=begin;
			this.end=end;
			this.tab=tab;
		}
		public void run()
        //surdéfinition de la méthode run() de la classe thread qui va s'exécuter à l'appel de la méthode start()
		{
			System.out.println("Nouveau thread entame son execution");
			T elem = this.tab[index]; //élément dont on cherche le doublon
			int i = this.begin ;
			while (i<this.end)
			{
				if (found==false && tab[i] == elem && i != index){
                    //la 2e condition sert à ne pas s'arrêter sur l'élément recherché si le thread couvre la partie du tableau le contenant
					System.out.println("Element trouvé, son indice est "+ i);
					found=true;
				}
				else{
					i++;
				}
			}
		}
	}
	
	private static <T> boolean isDuplicate(T tab[],int index, int k)
	{
		if (index >= tab.length){
			System.out.println("Index renvoyant à un indice hors du tableau");
			return false;
		}
		if (k > tab.length){
			System.out.println("Nombre de threads dépassant a taille du tableau");
			return false;
		}
		found = false;
		
		int size = tab.length;
		int begin=0;
		ThreadTri list[] = new ThreadTri[k] ;
		//tableau pour stocker les threads
		
		//boucle qui se charge de la division en sous-tableaux et du lancement des threads
		for(int i=0;i<k;i++){
			int res=size/(k-i); //taille de la fraction du tableau soumise au thread
			ThreadTri <T> x = new ThreadTri<T> (index,begin,begin+res,tab);
			x.start();
			list[i]=x;
			begin+=res;
			size-=res;
		}
		try{
			for(int i=0; i<k;i++){
                //boucle qui permet d'attendre la fin de tous les threads pour poursuivre l'exécution du thread principal
				list[i].join();
			}
		}
		catch(InterruptedException exc) {}
		return found;
	}
	
	private static <T> boolean isDuplicate(T tab[],int index)
    //surcharge de la méthode isDuplicate pour simuler un paramètre par défaut
	{
		return isDuplicate(tab,index,4);
	}
	
	public static <T> void main (String[] args) {
		
		Integer [] a  = {1,45,67,3,5,8,245,567,31,45,23,56,34,23,78,12,34,45,56,23,45,78,87,42,52,63,52,31,52,82};
		//Tableau d'entiers
		Character [] b = {'A', 'Z','R', 'T', 'H', 'J', 'B', 'n', 'u', 'Z', 'o','_', ')', 'A', 'k', 'h','b','b','o','p'};
		//Tableau de caractères
		Double [] c = {1.09,7.90,6.5,4.3,3.4,2.4,7.9,0.7,7.9};
		//Tableau de doubles
		
		System.out.println("Tableau d'entiers de taille "+ a.length);
		System.out.println("Nous cherchons l'élément d'indice 8 en 3 threads ");
		System.out.println(isDuplicate(a,8,3));
		
		System.out.println("Tableau de caractères de taille "+ b.length);
		System.out.println("Nous cherchons l'élément d'indice 2 avec un nombre de threads par défaut");
		System.out.println(isDuplicate(b,2));
		
		System.out.println("Tableau de doubles de taille "+ c.length);
		System.out.println("Nous cherchons l'élément d'indice 6 en 5 threads");
		System.out.println(isDuplicate(c,6,5));
		
	}
}

